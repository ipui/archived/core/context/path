# @ipui/path

ipui path context

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## ip[fn]s links

## description

provide a set of tools to parse `window.location` to work with ipui.

## technologies

* [reactjs](https://reactjs.org)
* [ipfs](https://ipfs.io)

## depends on

> this is a high order context.

## how it works

* it uses the [context api](https://reactjs.org/docs/context.html) of reactjs to provide an accessible service to any component inside the context itself.

* use RouteTools to perform hash url parser and navigation tasks.

* binds `withPath` on the `props` or `this.props` when is used as _consumer_.

## api

this api variables and methods are accesible by the component that uses `withPath` consumer.

### api

api network multiaddress (`/ip4/127.0.0.1/tcp/5001`).

### current

current path `/.conf/vimrc`.

this could work with [path-to-regexp](https://www.npmjs.com/package/path-to-regexp) to extend its capabilities.

## methods

this methods are accesible by the component that uses `withPath` consumer.

### go

> navigate between paths

**go( `path`, `options` )**

* `path` any string path such as `/me/friend/0/photo`
* `options`
  * `reload` force reload. boolean defaults to `false`.

### redirect

> alias of `go` but with `reload` defaults to `true`.

**redirect( `path` )**

* `path` is an url string.

### dirname

> get the `dirname` prefix of a given path.

**dirname( `path` )**

* `path` url string

### filename

> get the `filename` of a given path.

**filename( `path` )**

* `path` url string

### join

> like [path.join](https://nodejs.org/api/all.html#path_path_join_paths)

## usage

### provider

```javascript
import Path from '@ipui/path'

const MyComponent = props => {

  return (
    <Path>

      { /*
        your app code that requires a path provider
      */}

    </Path>
  )
}

export default MyComponent
```

### consumer

`DirGoUp` component example

```javascript
import React from 'react'

import { withPath } from '@ipui/path'

import { FiCornerLeftUp } from 'react-icons/fi'

const DirGoUp = props => {

  /* functional cd */
  function cd( dir ) {
    const { dirname, join, current, go } = this.props.withPath
    if ( dir === '..' )
      dir = dirname( current )
    else
      dir = join( current, dir )

    go( dir )
  }

  /* when click just go up */
  function handleClick() {
    cd( '..' )
  }

  const { current } = props.withPath

  return (
    <>
      { current !== '/' ? (
        <button onClick={ handleClick }>
          <FiCornerLeftUp />
        </button>
      ) : null }
    </>
  )
}

export default withPath( DirGoUp )
```

## last updated
aug 26, 2019
