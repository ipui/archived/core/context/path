import React, { createContext, Component } from 'react'
import RouteTools from './lib/RouteTools'

const PathContext = createContext()

class Path extends Component {

  state = {
    current: '/',
    api: '/ip4/127.0.0.1/tcp/5001'
  }

  constructor( props ) {
    super( props )
    this.go = this.go.bind( this )
  }

  componentDidMount() {
    const { api, app } = RouteTools.parse().addresses
    this.setState( {
      ...this.state,
      api: api,
      current: app
    } )
  }

  go( address, options ) {
    RouteTools.go( address, options || {} )
    this.setState( {
      ...this.state,
      current: address
    } )
  }

  render() {
    const { api, current } = this.state
    const { go } = this
    const { children } = this.props
    const { join, filename, dirname, redirect } = RouteTools

    return (
      <PathContext.Provider value={ {
        api,
        current,
        go,
        redirect,
        dirname,
        filename,
        join
      } } >
        { children }
      </PathContext.Provider>
    )
  }

}

const withPath = ( ComponentAlias ) => {

  return props => (
    <PathContext.Consumer>
      { context => {
        return <ComponentAlias { ...props } withPath={ context } />
      } }
    </PathContext.Consumer>
  )

}

export default Path

export {
  PathContext,
  withPath
}
