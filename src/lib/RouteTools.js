class RouteTools {

  static DEFAULT_API_ADDRESS = '/ip4/127.0.0.1/tcp/5001'
  static DEFAULT_HASH = '#'

  static hash() {
    return window.location.hash
  }

  static dirname( address ) {
    const _parts = address.split( '/' )
    _parts.pop()
    return RouteTools.clean( _parts.join( '/' ) )
  }

  static filename( address ) {
    return address.split( '/' ).pop()
  }

  static parseHash( hash ) {
    const apiAddress = hash.match( /(\/[^/\s]+){4}/ ).shift()

    const appAddress = hash.split( apiAddress ).pop() || '/'

    return {
      addresses: {
        api: RouteTools.clean( apiAddress ),
        app: RouteTools.clean( appAddress )
      }
    }

  }

  static clean( address ) {
    if ( !address )
      return '/'

    if ( address === '/' )
      return address

    const decoded = decodeURIComponent( address )

    return '/' + decoded.replace( /[/]+$/, '' ).replace( /^[/]+/, '' )
  }

  static parse() {
    const hash = RouteTools.hash()

    if ( !hash )
      return false

    return RouteTools.parseHash( hash )

  }

  static applyHash( address ) {
    return RouteTools.DEFAULT_HASH + address
  }

  static _go( path, options ) {
    window.location.replace( path )

    if ( options.reload )
      window.location.reload()
  }

  static go( path, options ) {
    const newHash = RouteTools.concat( path )
    RouteTools._go( newHash, options )
  }

  static concat( path ) {
    const parsed = RouteTools.parse()
    let apiAddress = RouteTools.DEFAULT_API_ADDRESS
    if ( parsed )
      apiAddress = parsed.addresses.api

    return RouteTools.applyHash( apiAddress + path )
  }

  static join( ...parts ) {
    return parts.reduce( ( cumulativePath, currentPath ) => {
      const cleanPath = RouteTools.clean( currentPath )
      if ( cleanPath === '/' )
        return cumulativePath

      cumulativePath.push( cleanPath )
      return cumulativePath
    }, [] ).join( '' )
  }

  static redirect( path ) {
    RouteTools.go( path, { reload: true } )
  }

}

export default RouteTools
